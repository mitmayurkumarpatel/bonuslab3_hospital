package hospital;

/**
 *
 * @author mitpa
 */
public class ChildWristband extends Band {
    
    private String parentName;

    public ChildWristband(String parentName, String code, String info) {
        super(code, info);
        this.parentName = parentName;
    }

    public String getParentName() {
        return parentName;
    }

   
    
}
