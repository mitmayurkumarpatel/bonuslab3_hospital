package hospital;

/**
 *
 * @author mitpa
 */
public class Doctor extends Person{
    
    private String doctor;

    public Doctor(String nameofDoctor, String name) {
        super(name);
        this.doctor = nameofDoctor;
    }

    public String getdoctor() {
        return doctor;
    }
    
    
    
}
