package hospital;

import java.util.ArrayList;

/**
 *
 * @author mitpa
 */
public class ResearchGroup {
    
     private ArrayList<Patient> patients = new ArrayList<Patient>();

    public ArrayList<Patient> getPatients() {
        return patients;
    }

    public ResearchGroup(ArrayList<Patient> pa) {
        this.patients=pa;
    }

  
}