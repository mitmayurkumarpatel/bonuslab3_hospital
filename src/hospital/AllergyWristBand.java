package hospital;

/**
 *
 * @author mitpa
 */
public class AllergyWristBand extends Band{
    
    private String allergy;

    public AllergyWristBand(String allergy, String code, String info) {
        super(code, info);
        this.allergy = allergy;
    }

    public String getAllergy() {
        return allergy;
    }

   
    
    
}
 

