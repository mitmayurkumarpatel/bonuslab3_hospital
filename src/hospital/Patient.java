package hospital;

/**
 *
 * @author mitpa
 */
public class Patient extends Person {
    
    private String dob;
    private String doctor;
    private Band wristband;

    public Patient(String dob, String doctor, Band wristband, String name) {
        super(name);
        this.dob = dob;
        this.doctor = doctor;
        this.wristband = wristband;
    }
    
    //not used setter to follow encapsulation
    public String getDob() {
        return dob;
    }

    public String getDoctor() {
        return doctor;
    }

   

   

    public Band getWristband() {
        return wristband;
    }
    
    
}
