package hospital;

/**
 *
 * @author mitpa
 */
public class Band 
{
    
    private String code;
    private String info;

    public Band(String code, String info) {
        this.code = code;
        this.info = info;
    }

    //used getter to follow rules of eencapsulation
    public String getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }

  

 
   
    
    
}
