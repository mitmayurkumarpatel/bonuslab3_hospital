package hospital;

/**
 *
 * @author mitpa
 */
public class Parent extends Person {
    private String parent;

    public Parent(String parent, String name) {
        super(name);
        this.parent = parent;
    }

   
    //getter
    //not used setter to follow rules of encapsulation
    public String getParent() {
        return parent;
    }
    
    
    
}
